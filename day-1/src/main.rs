use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let counter = run(contents.clone(), 1);
    println!("{}", counter);
    let counter = run(contents.clone(), 3);
    println!("{}", counter);
}

fn run(contents: String, range: usize) -> u32 {
    let lines: Vec<u32> = contents
        .trim()
        .split("\n")
        .map(|s| s.parse::<u32>().unwrap())
        .collect();
    let mut counter = 0;
    for x in range..lines.len() {
        if lines[x] > lines[x - range] {
            counter += 1;
        }
    }
    return counter;
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day1_1() {
        assert_eq!(
            run(
                "199\n200\n208\n210\n200\n207\n240\n269\n260\n263".to_string(),
                1
            ),
            7
        )
    }
    #[test]
    fn test_day1_2() {
        assert_eq!(
            run(
                "199\n200\n208\n210\n200\n207\n240\n269\n260\n263".to_string(),
                3
            ),
            5
        )
    }
}

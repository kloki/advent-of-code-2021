use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("error score {}", run_1(contents.clone()));
    println!("completion score {}", run_2(contents));
}
fn parse_line(line: &str) -> usize {
    let mut stack: Vec<char> = Vec::new();
    for c in line.chars() {
        match c {
            ')' => {
                if stack.pop().unwrap_or_default() != '(' {
                    return 3;
                }
            }
            ']' => {
                if stack.pop().unwrap_or_default() != '[' {
                    return 57;
                }
            }
            '}' => {
                if stack.pop().unwrap_or_default() != '{' {
                    return 1197;
                }
            }
            '>' => {
                if stack.pop().unwrap_or_default() != '<' {
                    return 25137;
                }
            }
            _ => stack.push(c),
        }
    }
    0
}
fn complete_line(line: &str) -> usize {
    let mut stack: Vec<char> = Vec::new();
    for c in line.chars() {
        match c {
            ')' | ']' | '}' | '>' => {
                stack.pop();
            }
            _ => stack.push(c),
        }
    }
    let mut score = 0;
    loop {
        match stack.pop() {
            Some('(') => score = score * 5 + 1,
            Some('[') => score = score * 5 + 2,
            Some('{') => score = score * 5 + 3,
            Some('<') => score = score * 5 + 4,
            _ => break,
        }
    }
    score
}
fn run_1(contents: String) -> usize {
    let lines: Vec<&str> = contents.trim().split("\n").collect();
    lines.iter().map(|l| parse_line(l)).sum()
}
fn run_2(contents: String) -> usize {
    let lines: Vec<&str> = contents.trim().split("\n").collect();
    let mut completions: Vec<usize> = lines
        .iter()
        .filter(|l| parse_line(l) == 0)
        .map(|l| complete_line(l))
        .collect();
    completions.sort();
    completions[completions.len() / 2]
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day10_1() {
        assert_eq!(run_1(input()), 26397)
    }
    #[test]
    fn test_day10_2() {
        assert_eq!(run_2(input()), 288957)
    }
    fn input() -> String {
        "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
"
        .to_string()
    }
}

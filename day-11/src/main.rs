use octopus::{get_neighbors, Octopus};
use std::env;
use std::fs;
mod octopus;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("flashes {}", run_1(contents.clone(), 100));
    println!("all_flash {}", run_2(contents));
    // println!("completion score {}", run_2(contents));
}

fn process_flashes(octi: &mut Vec<Vec<Octopus>>) -> usize {
    let mut flashes = 0;
    let width = octi[0].len();
    let length = octi.len();
    for y in 0..width {
        for x in 0..length {
            if octi[x][y].flash() {
                flashes += 1;
                get_neighbors(x, y, width, length)
                    .into_iter()
                    .for_each(|(x, y)| octi[x][y].neighbor_flash())
            }
        }
    }
    if flashes > 0 {
        flashes += process_flashes(octi);
    }

    flashes
}
#[allow(dead_code)]
fn display_octi(octi: &Vec<Vec<Octopus>>) {
    let mut board: String = "".to_string();
    for line in octi {
        for o in line {
            board.push_str(&o.energy.to_string())
        }
        board.push_str(&"\n");
    }
    println!("{}", board);
}

fn all_flash(octi: &Vec<Vec<Octopus>>) -> bool {
    if octi
        .iter()
        .map(|x| x.iter().map(|x| x.energy).sum::<usize>())
        .sum::<usize>()
        == 0usize
    {
        return true;
    }
    false
}
fn run_1(contents: String, steps: usize) -> usize {
    let mut octi: Vec<Vec<Octopus>> = contents
        .trim()
        .split("\n")
        .map(|s| {
            s.split("")
                .filter(|x| x != &"")
                .map(|x| Octopus::new(x.parse::<usize>().unwrap()))
                .collect::<Vec<Octopus>>()
        })
        .collect();
    let mut flashes = 0;
    for _ in 0..steps {
        octi.iter_mut()
            .for_each(|l| l.into_iter().for_each(|o| o.step()));
        flashes += process_flashes(&mut octi);
    }

    flashes
}
fn run_2(contents: String) -> usize {
    let mut octi: Vec<Vec<Octopus>> = contents
        .trim()
        .split("\n")
        .map(|s| {
            s.split("")
                .filter(|x| x != &"")
                .map(|x| Octopus::new(x.parse::<usize>().unwrap()))
                .collect::<Vec<Octopus>>()
        })
        .collect();
    let mut step = 0;
    loop {
        step += 1;
        octi.iter_mut()
            .for_each(|l| l.into_iter().for_each(|o| o.step()));
        process_flashes(&mut octi);
        if all_flash(&octi) {
            return step;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day11_1_10() {
        assert_eq!(run_1(input(), 10), 204)
    }
    #[test]
    fn test_day11_1_100() {
        assert_eq!(run_1(input(), 100), 1656)
    }
    #[test]
    fn test_day11_2() {
        assert_eq!(run_2(input()), 195)
    }
    fn input() -> String {
        "5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"
            .to_string()
    }
}

#[derive(Debug)]
pub struct Octopus {
    pub energy: usize,
}

impl Octopus {
    pub fn new(energy: usize) -> Self {
        Octopus { energy }
    }
    pub fn step(&mut self) {
        self.energy += 1;
    }
    pub fn flash(&mut self) -> bool {
        if self.energy > 9 {
            self.energy = 0;
            return true;
        }
        false
    }
    pub fn neighbor_flash(&mut self) {
        if self.energy != 0 {
            self.energy += 1;
        }
    }
}
pub fn get_neighbors(x: usize, y: usize, width: usize, height: usize) -> Vec<(usize, usize)> {
    let mut neighbors: Vec<(usize, usize)> = Vec::new();
    if x != 0 {
        neighbors.push((x - 1, y));
        if y != 0 {
            neighbors.push((x - 1, y - 1));
        }
        if y != width - 1 {
            neighbors.push((x - 1, y + 1));
        }
    }
    if x != height - 1 {
        neighbors.push((x + 1, y));
        if y != 0 {
            neighbors.push((x + 1, y - 1));
        }
        if y != width - 1 {
            neighbors.push((x + 1, y + 1));
        }
    }
    if y != 0 {
        neighbors.push((x, y - 1));
    }
    if y != width - 1 {
        neighbors.push((x, y + 1));
    }

    neighbors
}

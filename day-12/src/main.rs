use std::collections::HashMap;
use std::env;
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Number of paths {}", run(contents.clone(), false));
    println!(
        "Number of paths plus extra small cave {}",
        run(contents, true)
    );
}

fn parse_caves<'input>(contents: &'input str) -> HashMap<&'input str, Vec<&'input str>> {
    let lines: Vec<&str> = contents.trim().split("\n").collect();
    let mut caves: HashMap<&str, Vec<&str>> = HashMap::new();
    for line in lines {
        let c: Vec<&str> = line.split("-").collect();
        let c1 = caves.entry(&c[0]).or_insert(Vec::new());
        c1.push(&c[1]);
        let c2 = caves.entry(&c[1]).or_insert(Vec::new());
        c2.push(&c[0]);
    }
    caves
}

fn find_path<'input>(
    caves: &HashMap<&'input str, Vec<&'input str>>,
    current_path: Vec<&'input str>,
    extra_small_cave: bool,
) -> Vec<Vec<&'input str>> {
    let mut paths: Vec<Vec<&str>> = Vec::new();

    for path in &caves[current_path[current_path.len() - 1]] {
        let mut new_path = current_path.to_vec();
        new_path.push(path);
        if path == &"end" {
            paths.push(new_path)
        } else if path.chars().collect::<Vec<char>>()[0].is_uppercase()
            || !current_path.contains(path)
        {
            paths.append(&mut find_path(&caves, new_path, extra_small_cave));
        } else if path != &"start" && extra_small_cave {
            paths.append(&mut find_path(&caves, new_path, false));
        }
    }
    paths
}

fn run(contents: String, extra_small_cave: bool) -> usize {
    let caves = parse_caves(&contents);
    let paths = find_path(&caves, vec!["start"], extra_small_cave);
    paths.len()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day12_1_1() {
        let input = "start-A
start-b
A-c
A-b
b-d
A-end
b-end"
            .to_string();
        assert_eq!(run(input, false), 10)
    }
    #[test]
    fn test_day12_1_2() {
        let input = "dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc"
            .to_string();
        assert_eq!(run(input, false), 19)
    }
    #[test]
    fn test_day12_1_3() {
        let input = "fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW"
            .to_string();
        assert_eq!(run(input, false), 226)
    }
    #[test]
    fn test_day12_2() {
        let input = "start-A
start-b
A-c
A-b
b-d
A-end
b-end"
            .to_string();
        assert_eq!(run(input, true), 36)
    }
}

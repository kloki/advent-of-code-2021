use std::env;
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let (holes, folds) = parse_contents(contents);
    println!(
        "1 Fold {} holes",
        run(holes.to_vec(), folds[..1].to_vec()).len()
    );

    let folded = run(holes.to_vec(), folds.to_vec());
    display_screen(&folded);
}

#[derive(Debug, Copy, Clone)]
pub enum Fold {
    X(usize),
    Y(usize),
}
fn display_screen(holes: &Vec<(usize, usize)>) {
    let max_x = holes.iter().map(|(x, _)| x).fold(0usize, |m, x| m.max(*x)) + 1;
    let max_y = holes.iter().map(|(_, y)| y).fold(0usize, |m, y| m.max(*y)) + 1;
    let mut screen = vec![vec![" "; max_x]; max_y];
    for (x, y) in holes {
        screen[*y][*x] = "#"
    }
    for line in screen {
        println!("{}", line.join(""))
    }
}
fn parse_contents(contents: String) -> (Vec<(usize, usize)>, Vec<Fold>) {
    let parts: Vec<_> = contents.trim().split("\n\n").collect();
    let mut holes: Vec<(usize, usize)> = Vec::new();
    for line in parts[0].split("\n") {
        let items: Vec<_> = line.split(",").collect();
        holes.push((
            items[0].parse::<usize>().unwrap(),
            items[1].parse::<usize>().unwrap(),
        ))
    }

    let mut folds: Vec<Fold> = Vec::new();
    for line in parts[1].split("\n") {
        let items: Vec<_> = line.split("=").collect();
        let number: usize = items[1].parse().unwrap();
        match items[0] {
            "fold along y" => folds.push(Fold::Y(number)),
            "fold along x" => folds.push(Fold::X(number)),
            _ => panic!("unknown input"),
        }
    }
    (holes, folds)
}

fn run(mut holes: Vec<(usize, usize)>, folds: Vec<Fold>) -> Vec<(usize, usize)> {
    for fold in folds {
        match fold {
            Fold::X(s) => {
                for h in holes.iter_mut() {
                    if h.0 > s {
                        h.0 = s - (h.0 - s)
                    }
                }
            }
            Fold::Y(s) => {
                for h in holes.iter_mut() {
                    if h.1 > s {
                        h.1 = s - (h.1 - s)
                    }
                }
            }
        }
        holes.sort();
        holes.dedup()
    }
    holes
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day13_1() {
        let (holes, folds) = parse_contents(input());
        assert_eq!(run(holes, folds[..1].to_vec()).len(), 17)
    }
    #[test]
    fn test_day13_2() {
        let (holes, folds) = parse_contents(input());
        assert_eq!(run(holes, folds.to_vec()).len(), 16)
    }

    fn input() -> String {
        "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5"
            .to_string()
    }
}

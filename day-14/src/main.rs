use std::collections::HashMap;
use std::env;
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Score after 10 steps: {}", run(contents.clone(), 10));
    println!("Score after 40 steps: {}", run(contents.clone(), 40));
}

fn parse_contents(contents: String) -> (HashMap<(char, char), char>, Vec<char>) {
    let parts: Vec<_> = contents.trim().split("\n\n").collect();
    let input = parts[0].chars().collect();
    let mut template: HashMap<(char, char), char> = HashMap::new();
    for line in parts[1].split("\n") {
        let ch: Vec<char> = line.replace(" -> ", "").chars().collect();
        template.insert((ch[0], ch[1]), ch[2]);
    }
    (template, input)
}
fn process_polymer(
    template: &HashMap<(char, char), char>,
    pairs: HashMap<(char, char), usize>,
) -> HashMap<(char, char), usize> {
    let mut new_pairs: HashMap<(char, char), usize> = HashMap::new();
    for pair in pairs.keys() {
        let m = template.get(pair).unwrap();
        let value = pairs.get(pair).unwrap();
        let count = new_pairs.entry((pair.0, *m)).or_insert(0);
        *count += value;
        let count = new_pairs.entry((*m, pair.1)).or_insert(0);
        *count += value;
    }

    new_pairs
}
fn run(contents: String, steps: usize) -> usize {
    let (template, mut polymer) = parse_contents(contents);
    // preset the edges
    let mut scores: HashMap<char, usize> = HashMap::new();
    let count = scores.entry(polymer[0]).or_insert(0);
    *count += 1;
    let count = scores.entry(polymer[polymer.len() - 1]).or_insert(0);
    *count += 1;

    let mut prev = polymer[0];
    let mut current: char;
    polymer.remove(0);
    // get first pairs
    let mut pairs: HashMap<(char, char), usize> = HashMap::new();
    for x in &polymer {
        current = *x;
        let count = pairs.entry((prev, current)).or_insert(0);
        *count += 1;
        prev = current;
    }

    // actual pairs
    for _ in 0..steps {
        pairs = process_polymer(&template, pairs);
    }
    // extract scores
    for pair in pairs.keys() {
        let value = pairs.get(pair).unwrap();
        let count = scores.entry(pair.0).or_insert(0);
        *count += value;
        let count = scores.entry(pair.1).or_insert(0);
        *count += value;
    }
    let mut score_list: Vec<usize> = scores.values().map(|x| x / 2).collect();
    score_list.sort();
    score_list[score_list.len() - 1] - score_list[0]
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day14_1() {
        assert_eq!(run(input(), 10), 1588)
    }
    #[test]
    fn test_day14_2() {
        assert_eq!(run(input(), 40), 2188189693529)
    }

    fn input() -> String {
        "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"
            .to_string()
    }
}

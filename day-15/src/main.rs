use node::Node;
use std::env;
use std::fs;
mod node;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("risk {}", run(contents.clone(), 1));
    println!("risk * 5 {}", run(contents.clone(), 5));
}

pub fn get_neighbors(x: usize, y: usize, width: usize, lenght: usize) -> Vec<(usize, usize)> {
    let mut neighbors: Vec<(usize, usize)> = Vec::new();
    if x != 0 {
        neighbors.push((x - 1, y));
    }
    if x != lenght - 1 {
        neighbors.push((x + 1, y));
    }
    if y != 0 {
        neighbors.push((x, y - 1));
    }
    if y != width - 1 {
        neighbors.push((x, y + 1));
    }

    neighbors
}

fn find_index(x: usize, y: usize, width: usize) -> usize {
    x * width + y
}
fn run(contents: String, multiplier: usize) -> usize {
    let lines: Vec<&str> = contents.split("\n").collect();
    let mut width = lines[0].chars().count();
    let mut length = lines.len();
    let mut cavern: Vec<Node> = Vec::new();
    for a in 0..multiplier {
        for (x, line) in lines.iter().enumerate() {
            for b in 0..multiplier {
                for (y, char) in line.chars().enumerate() {
                    let mut risk: usize = char.to_digit(10).unwrap() as usize;
                    risk = risk + a + b;
                    if risk > 9 {
                        risk -= 9
                    }
                    cavern.push(Node::new(x + a * length, y + b * width, risk))
                }
            }
        }
    }
    width = width * multiplier;
    length = length * multiplier;

    let mut current_x: usize = 0;
    let mut current_y: usize = 0;
    let mut current_risk: usize = 0;
    loop {
        if current_x == length - 1 && current_y == width - 1 {
            break;
        }
        for (x, y) in get_neighbors(current_x, current_y, width, length) {
            &cavern[find_index(x, y, width)].touch(current_risk);
        }
        &cavern[find_index(current_x, current_y, width)].visit();
        let node = cavern.iter().filter(|x| !x.visited).min().unwrap();
        current_x = node.x;
        current_y = node.y;
        current_risk = node.total_risk;
    }
    cavern[find_index(current_x, current_y, width)].total_risk
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day15_0() {
        let input = "12
13"
        .to_string();
        assert_eq!(run(input, 1), 4)
    }
    #[test]
    fn test_day15_1_5() {
        let input_1 = "19
13"
        .to_string();
        let input_2 = "1921
1324
2132
2435"
            .to_string();
        assert_eq!(run(input_1, 2), run(input_2, 1))
    }
    #[test]
    fn test_day15_1() {
        assert_eq!(run(input(), 1), 40)
    }
    #[test]
    fn test_day15_2() {
        assert_eq!(run(input(), 5), 315)
    }
    fn input() -> String {
        "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581"
            .to_string()
    }
}

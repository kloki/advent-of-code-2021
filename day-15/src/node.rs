use std::cmp::Ordering;
#[derive(Debug, Eq)]
pub struct Node {
    pub x: usize,
    pub y: usize,
    pub risk: usize,
    pub total_risk: usize,
    pub visited: bool,
}

impl Node {
    pub fn new(x: usize, y: usize, risk: usize) -> Self {
        Node {
            x,
            y,
            risk,
            total_risk: 999999,
            visited: false,
        }
    }
    pub fn touch(&mut self, risk: usize) {
        if (self.risk + risk) < self.total_risk {
            self.total_risk = self.risk + risk
        }
    }
    pub fn visit(&mut self) {
        self.visited = true
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> Ordering {
        self.total_risk.cmp(&other.total_risk)
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.total_risk == other.total_risk
    }
}

use ::std::convert::TryInto;
use packet::Packet;
use std::env;
use std::fs;
mod packet;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Sum version number {}", run(contents.clone()));
    println!("Computation_results {}", run_2(contents.clone()));
}
fn parse_input(contents: String) -> Vec<u8> {
    let mut input: Vec<u8> = Vec::new();
    for c in contents.trim().chars().rev() {
        match c {
            '0' => input.append(&mut vec![0, 0, 0, 0]),
            '1' => input.append(&mut vec![1, 0, 0, 0]),
            '2' => input.append(&mut vec![0, 1, 0, 0]),
            '3' => input.append(&mut vec![1, 1, 0, 0]),
            '4' => input.append(&mut vec![0, 0, 1, 0]),
            '5' => input.append(&mut vec![1, 0, 1, 0]),
            '6' => input.append(&mut vec![0, 1, 1, 0]),
            '7' => input.append(&mut vec![1, 1, 1, 0]),
            '8' => input.append(&mut vec![0, 0, 0, 1]),
            '9' => input.append(&mut vec![1, 0, 0, 1]),
            'A' => input.append(&mut vec![0, 1, 0, 1]),
            'B' => input.append(&mut vec![1, 1, 0, 1]),
            'C' => input.append(&mut vec![0, 0, 1, 1]),
            'D' => input.append(&mut vec![1, 0, 1, 1]),
            'E' => input.append(&mut vec![0, 1, 1, 1]),
            'F' => input.append(&mut vec![1, 1, 1, 1]),
            _ => {}
        }
    }
    input
}
fn find_number(input: &mut Vec<u8>, bits: usize) -> usize {
    let mut x: usize = 0;
    for i in 1..bits + 1 {
        let bit = input.pop().unwrap() as usize;
        let p: u32 = (bits - i).try_into().unwrap();
        let multiplier: usize = 2_usize.pow(p);
        x += bit * multiplier;
    }
    x
}
fn find_literal(input: &mut Vec<u8>) -> usize {
    let mut bits: Vec<u8> = Vec::new();
    loop {
        let leading = input.pop().unwrap();

        bits.push(input.pop().unwrap());
        bits.push(input.pop().unwrap());
        bits.push(input.pop().unwrap());
        bits.push(input.pop().unwrap());
        if leading == 0 {
            break;
        }
    }
    let mut x: usize = 0;
    let size = bits.len();
    for (i, bit) in bits.iter().enumerate() {
        let p: u32 = (size - 1 - i).try_into().unwrap();
        let multiplier = 2usize.pow(p);
        let b = *bit as usize;
        x += b * multiplier;
    }
    x
}

fn parse_packets(input: &mut Vec<u8>, packets: &mut Vec<Packet>) -> usize {
    let version = find_number(input, 3);
    let type_id = find_number(input, 3);
    let mut result = 0;
    if type_id == 4 {
        result = find_literal(input);
    } else {
        let ltid = input.pop().unwrap();
        let mut results: Vec<usize> = Vec::new();
        if ltid == 0 {
            let bitsize = find_number(input, 15) as usize;
            let target = input.len() - bitsize;
            loop {
                results.push(parse_packets(input, packets));
                if input.len() <= target {
                    break;
                }
            }
        } else {
            let amount = find_number(input, 11) as usize;
            for _ in 0..amount {
                results.push(parse_packets(input, packets));
            }
        }
        match type_id {
            0 => result = results.iter().sum(),
            1 => result = results.iter().fold(1usize, |p, v| p * v),
            2 => result = *results.iter().min().unwrap(),
            3 => result = *results.iter().max().unwrap(),
            5 => {
                if results[0] > results[1] {
                    result = 1;
                }
            }
            6 => {
                if results[0] < results[1] {
                    result = 1;
                }
            }
            7 => {
                if results[0] == results[1] {
                    result = 1;
                }
            }
            _ => {}
        }
    }
    packets.push(Packet::new(version, type_id, result));
    result
}
fn run(contents: String) -> usize {
    let mut input = parse_input(contents);
    let mut packets: Vec<Packet> = Vec::new();
    parse_packets(&mut input, &mut packets);
    packets.iter().map(|x| x.version).sum()
}
fn run_2(contents: String) -> usize {
    let mut input = parse_input(contents);
    let mut packets: Vec<Packet> = Vec::new();
    parse_packets(&mut input, &mut packets)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day16_1_1() {
        assert_eq!(run("8A004A801A8002F478".to_string()), 16)
    }
    #[test]
    fn test_day16_1_2() {
        assert_eq!(run("620080001611562C8802118E34".to_string()), 12)
    }
    #[test]
    fn test_day16_1_3() {
        assert_eq!(run("C0015000016115A2E0802F182340".to_string()), 23)
    }
    #[test]
    fn test_day16_1_4() {
        assert_eq!(run("A0016C880162017C3686B18A3D4780".to_string()), 31)
    }
    #[test]
    fn test_day16_2_1() {
        assert_eq!(run_2("C200B40A82".to_string()), 3)
    }
    #[test]
    fn test_day16_2_2() {
        assert_eq!(run_2("04005AC33890".to_string()), 54)
    }
    #[test]
    fn test_day16_2_3() {
        assert_eq!(run_2("880086C3E88112".to_string()), 7)
    }
    #[test]
    fn test_day16_2_4() {
        assert_eq!(run_2("CE00C43D881120".to_string()), 9)
    }
    #[test]
    fn test_day16_2_5() {
        assert_eq!(run_2("D8005AC2A8F0".to_string()), 1)
    }
    #[test]
    fn test_day16_2_6() {
        assert_eq!(run_2("F600BC2D8F".to_string()), 0)
    }
    #[test]
    fn test_day16_2_7() {
        assert_eq!(run_2("9C005AC2F8F0".to_string()), 0)
    }
    #[test]
    fn test_day16_2_8() {
        assert_eq!(run_2("9C0141080250320F1802104A08".to_string()), 1)
    }
}

#[derive(Debug)]
pub struct Packet {
    pub version: usize,
    pub type_id: usize,
    pub result: usize,
}
impl Packet {
    pub fn new(version: usize, type_id: usize, result: usize) -> Self {
        Packet {
            version,
            type_id,
            result,
        }
    }
}

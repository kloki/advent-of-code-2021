use std::env;
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Heighest {}", run_1(contents.clone()));
    println!("Distinct {}", run_2(contents.clone()));
}
fn parse_input(contents: String) -> (isize, isize, isize, isize) {
    let coor: Vec<isize> = contents
        .trim()
        .replace("target area: x=", "")
        .replace(", y=", "..")
        .split("..")
        .map(|x| x.parse::<isize>().unwrap())
        .collect();
    (coor[0], coor[1], coor[3], coor[2])
}
#[derive(Debug)]
struct ProbeShot {
    highest: isize,
    hit: bool,
}

fn simulate(
    x_vel: isize,
    y_vel: isize,
    x_min: isize,
    x_max: isize,
    y_min: isize,
    y_max: isize,
) -> ProbeShot {
    let mut x_location = 0;
    let mut x_velocity = x_vel;
    let mut y_location = 0;
    let mut y_velocity = y_vel;
    let mut highest = 0;
    loop {
        x_location += x_velocity;
        y_location += y_velocity;
        if x_velocity != 0 {
            x_velocity -= 1;
        }
        y_velocity -= 1;
        if y_location > highest {
            highest = y_location;
        }
        if x_location >= x_min && x_location <= x_max && y_location <= y_min && y_location >= y_max
        {
            return ProbeShot { highest, hit: true };
        }
        if x_location > x_max || y_location < y_max {
            return ProbeShot {
                highest,
                hit: false,
            };
        }
    }
}

fn run_1(contents: String) -> isize {
    let (x_min, x_max, y_min, y_max) = parse_input(contents);

    for y_velocity in (0..(y_max * -1)).rev() {
        for x_velocity in 0..x_max {
            let shot = simulate(x_velocity, y_velocity, x_min, x_max, y_min, y_max);
            if shot.hit {
                return shot.highest;
            }
        }
    }
    0
}
fn run_2(contents: String) -> usize {
    let (x_min, x_max, y_min, y_max) = parse_input(contents);
    let mut shots: Vec<ProbeShot> = Vec::new();
    for y_velocity in (y_max..(y_max * -1) + 1).rev() {
        for x_velocity in 0..x_max + 1 {
            shots.push(simulate(x_velocity, y_velocity, x_min, x_max, y_min, y_max));
        }
    }
    shots.retain(|x| x.hit);
    shots.len()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day17_0() {
        assert_eq!(simulate(9, 0, 20, 30, -5, -10).hit, true)
    }
    #[test]
    fn test_day17_1() {
        assert_eq!(run_1(input()), 45)
    }
    #[test]
    fn test_day17_2() {
        assert_eq!(run_2(input()), 112)
    }
    fn input() -> String {
        "target area: x=20..30, y=-10..-5".to_string()
    }
}

use std::env;
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Magnitude {}", run(contents.clone()));
    println!("Highest sum {}", run_2(contents.clone()));
}

fn parse_number(content: &str) -> Vec<(usize, usize)> {
    let mut depth: usize = 0;
    let mut number: Vec<(usize, usize)> = Vec::new();
    for x in content.split(",") {
        match x {
            "[" => depth += 1,
            "]" => depth -= 1,
            _ => number.push((x.parse::<usize>().unwrap(), depth - 1)),
        }
    }
    number
}

fn parse_input(contents: String) -> Vec<Vec<(usize, usize)>> {
    let mut numbers: Vec<Vec<(usize, usize)>> = contents
        .trim()
        .replace("[", "[,")
        .replace("]", ",]")
        .split("\n")
        .map(|x| parse_number(x))
        .collect();

    let mut numbers_rev: Vec<Vec<(usize, usize)>> = Vec::new();
    while numbers.len() != 0 {
        numbers_rev.push(numbers.pop().unwrap())
    }
    numbers_rev
}
fn explode(sum: &mut Vec<(usize, usize)>) -> bool {
    for x in 0..sum.len() - 1 {
        if sum[x].1 == 4 && sum[x + 1].1 == 4 {
            if x != 0 {
                sum[x - 1].0 += sum[x].0
            }
            if x + 2 < sum.len() {
                sum[x + 2].0 += sum[x + 1].0
            }
            sum[x].0 = 0;
            sum[x].1 -= 1;
            sum.remove(x + 1);
            return true;
        }
    }
    false
}
fn split(sum: &mut Vec<(usize, usize)>) -> bool {
    for x in 0..sum.len() {
        if sum[x].0 > 9 {
            sum.insert(x + 1, ((sum[x].0 + 1) / 2, sum[x].1 + 1));
            sum[x].1 += 1;
            sum[x].0 /= 2;
            return true;
        }
    }
    false
}

fn magnitude_node(sum: &mut Vec<(usize, usize)>, depth: usize) -> bool {
    for x in 0..sum.len() - 1 {
        if sum[x].1 == depth && sum[x + 1].1 == depth {
            sum[x].0 = sum[x].0 * 3 + sum[x + 1].0 * 2;
            sum[x].1 -= 1;
            sum.remove(x + 1);
            return true;
        }
    }
    false
}
fn magnitude(sum: &mut Vec<(usize, usize)>) -> usize {
    for depth in (1..4).rev() {
        loop {
            if magnitude_node(sum, depth) {
                continue;
            }
            break;
        }
    }
    sum[0].0 * 3 + sum[1].0 * 2
}

fn run_2(contents: String) -> usize {
    let mut top_score = 0;
    let lines: Vec<_> = contents.split("\n").collect();
    for line_a in &lines {
        for line_b in &lines {
            if line_a != line_b {
                let score = run(format!("{}\n{}", line_a, line_b));
                if score > top_score {
                    top_score = score;
                }
            }
        }
    }
    top_score
}
fn run(contents: String) -> usize {
    let mut numbers: Vec<Vec<(usize, usize)>> = parse_input(contents);

    let mut sum = numbers.pop().unwrap();
    while numbers.len() != 0 {
        sum.append(&mut numbers.pop().unwrap());
        for x in &mut sum {
            x.1 += 1;
        }
        loop {
            if explode(&mut sum) {
                continue;
            }
            if split(&mut sum) {
                continue;
            }
            break;
        }
    }
    magnitude(&mut sum)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day18_0() {
        let input = "[[[[4,3],4],4],[7,[[8,4],9]]]
[1,1]"
            .to_string();
        assert_eq!(run(input), 1384)
    }

    #[test]
    fn test_day18_1() {
        assert_eq!(run(input()), 4140)
    }

    #[test]
    fn test_day18_2() {
        assert_eq!(run_2(input()), 3993)
    }

    fn input() -> String {
        "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"
            .to_string()
    }
}

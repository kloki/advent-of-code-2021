use movement::Movement;
use std::env;
use std::fs;
mod movement;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("{}", run_1(contents.clone()));
    println!("{}", run_2(contents.clone()));
}

fn run_1(contents: String) -> u32 {
    let lines: Vec<Movement> = contents
        .trim()
        .split("\n")
        .map(|s| s.parse::<Movement>().unwrap())
        .collect();
    let mut depth = 0;
    let mut distance = 0;
    for x in &lines {
        match x {
            Movement::UP(s) => depth -= s,
            Movement::DOWN(s) => depth += s,
            Movement::FORWARD(s) => distance += s,
        }
    }
    depth * distance
}

fn run_2(contents: String) -> u32 {
    let lines: Vec<Movement> = contents
        .trim()
        .split("\n")
        .map(|s| s.parse::<Movement>().unwrap())
        .collect();
    let mut depth = 0;
    let mut distance = 0;
    let mut aim = 0;
    for x in &lines {
        match x {
            Movement::UP(s) => aim -= s,
            Movement::DOWN(s) => aim += s,
            Movement::FORWARD(s) => {
                distance += s;
                depth += aim * s;
            }
        }
    }
    depth * distance
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day2_1() {
        assert_eq!(
            run_1("forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2".to_string()),
            150
        )
    }
    #[test]
    fn test_day2_2() {
        assert_eq!(
            run_2("forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2".to_string()),
            900
        )
    }
}

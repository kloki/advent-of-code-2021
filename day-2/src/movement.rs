use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug)]
pub enum Movement {
    UP(u32),
    DOWN(u32),
    FORWARD(u32),
}

impl FromStr for Movement {
    type Err = MovementError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (action, speed) = split_word(s).ok_or(MovementError::InvalidString)?;
        let speed: u32 = speed.parse()?;
        match action {
            "up" => Ok(Self::UP(speed)),
            "down" => Ok(Self::DOWN(speed)),
            "forward" => Ok(Self::FORWARD(speed)),
            _ => Err(MovementError::InvalidAction),
        }
    }
}

fn split_word(word: &str) -> Option<(&str, &str)> {
    for (i, c) in word.chars().enumerate() {
        if c == ' ' {
            return Some((&word[..i], &word[i + 1..]));
        }
    }

    None
}

#[derive(Debug)]
pub enum MovementError {
    InvalidString,
    InvalidSpeed,
    InvalidAction,
}
impl From<ParseIntError> for MovementError {
    fn from(_: ParseIntError) -> Self {
        Self::InvalidSpeed
    }
}

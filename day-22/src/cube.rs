#[derive(Debug)]
pub struct Cube {
    x_min: isize,
    x_max: isize,
    y_min: isize,
    y_max: isize,
    z_min: isize,
    z_max: isize,
}

impl Cube {
    pub fn new(
        x_min: isize,
        x_max: isize,
        y_min: isize,
        y_max: isize,
        z_min: isize,
        z_max: isize,
    ) -> Self {
        Cube {
            x_min,
            x_max,
            y_min,
            y_max,
            z_min,
            z_max,
        }
    }
    pub fn outside_init_region(&self) -> bool {
        self.x_min < -50
            || self.x_max > 50
            || self.y_min < -50
            || self.y_max > 50
            || self.z_min < -50
            || self.z_max > 50
    }

    pub fn size(&self) -> isize {
        (1 + self.x_max - self.x_min).abs()
            * (1 + self.y_max - self.y_min).abs()
            * (1 + self.z_max - self.z_min).abs()
    }
    pub fn disjoint(&self, other: Cube) -> Vec<Cube> {
        if self.x_min > other.x_max
            || self.x_max < other.x_min
            || self.y_min > other.y_max
            || self.y_max < other.y_min
            || self.z_min > other.z_max
            || self.z_max < other.z_min
        {
            return vec![other];
        }
        let mut cubes: Vec<Cube> = Vec::new();
        if other.x_min < self.x_min {
            cubes.push(Cube::new(
                other.x_min,
                self.x_min - 1,
                other.y_min,
                other.y_max,
                other.z_min,
                other.z_max,
            ))
        }
        if other.x_max > self.x_max {
            cubes.push(Cube::new(
                self.x_max + 1,
                other.x_max,
                other.y_min,
                other.y_max,
                other.z_min,
                other.z_max,
            ))
        }
        if other.y_min < self.y_min {
            cubes.push(Cube::new(
                other.x_min.max(self.x_min),
                other.x_max.min(self.x_max),
                other.y_min,
                self.y_min - 1,
                other.z_min,
                other.z_max,
            ))
        }
        if other.y_max > self.y_max {
            cubes.push(Cube::new(
                other.x_min.max(self.x_min),
                other.x_max.min(self.x_max),
                self.y_max + 1,
                other.y_max,
                other.z_min,
                other.z_max,
            ))
        }
        if other.z_min < self.z_min {
            cubes.push(Cube::new(
                other.x_min.max(self.x_min),
                other.x_max.min(self.x_max),
                other.y_min.max(self.y_min),
                other.y_max.min(self.y_max),
                other.z_min,
                self.z_min - 1,
            ))
        }
        if other.z_max > self.z_max {
            cubes.push(Cube::new(
                other.x_min.max(self.x_min),
                other.x_max.min(self.x_max),
                other.y_min.max(self.y_min),
                other.y_max.min(self.y_max),
                self.z_max + 1,
                other.z_max,
            ))
        }
        cubes
    }
}

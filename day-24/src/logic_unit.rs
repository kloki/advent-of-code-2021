use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug, PartialEq, Clone, Copy)]
enum Variable {
    X,
    Y,
    W,
    Z,
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum VariablePlus {
    X,
    Y,
    W,
    Z,
    INT(i64),
}
#[derive(Debug, PartialEq, Clone, Copy)]
enum Operation {
    INP(Variable),
    ADD(Variable, VariablePlus),
    MUL(Variable, VariablePlus),
    DIV(Variable, VariablePlus),
    MOD(Variable, VariablePlus),
    EQL(Variable, VariablePlus),
}

#[derive(Debug, PartialEq)]
pub enum LogicUnitError {
    InvalidVariableError,
    InvalidOperationError,
}
impl FromStr for VariablePlus {
    type Err = LogicUnitError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "x" => Ok(Self::X),
            "y" => Ok(Self::Y),
            "w" => Ok(Self::W),
            "z" => Ok(Self::Z),
            _ => Ok(Self::INT(s.parse()?)),
        }
    }
}
impl FromStr for Variable {
    type Err = LogicUnitError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "x" => Ok(Self::X),
            "y" => Ok(Self::Y),
            "w" => Ok(Self::W),
            "z" => Ok(Self::Z),
            _ => Err(LogicUnitError::InvalidVariableError),
        }
    }
}
impl From<ParseIntError> for LogicUnitError {
    fn from(_: ParseIntError) -> Self {
        Self::InvalidVariableError
    }
}

impl FromStr for Operation {
    type Err = LogicUnitError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<_> = s.trim().split(" ").collect();
        match parts[0] {
            "inp" => Ok(Self::INP(
                parts
                    .get(1)
                    .ok_or(LogicUnitError::InvalidVariableError)?
                    .parse::<Variable>()?,
            )),
            "add" => Ok(Self::ADD(
                parts
                    .get(1)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<Variable>()?,
                parts
                    .get(2)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<VariablePlus>()?,
            )),
            "mul" => Ok(Self::MUL(
                parts
                    .get(1)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<Variable>()?,
                parts
                    .get(2)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<VariablePlus>()?,
            )),
            "div" => Ok(Self::DIV(
                parts
                    .get(1)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<Variable>()?,
                parts
                    .get(2)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<VariablePlus>()?,
            )),
            "mod" => Ok(Self::MOD(
                parts
                    .get(1)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<Variable>()?,
                parts
                    .get(2)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<VariablePlus>()?,
            )),
            "eql" => Ok(Self::EQL(
                parts
                    .get(1)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<Variable>()?,
                parts
                    .get(2)
                    .ok_or(LogicUnitError::InvalidOperationError)?
                    .parse::<VariablePlus>()?,
            )),
            _ => Err(LogicUnitError::InvalidOperationError),
        }
    }
}

pub struct LogicUnit {
    operations: Vec<Operation>,
    input: Vec<i64>,
    x: i64,
    y: i64,
    w: i64,
    z: i64,
}

impl LogicUnit {
    pub fn new(s: &str) -> Result<Self, LogicUnitError> {
        let operations = s
            .trim()
            .split("\n")
            .map(|x| x.parse::<Operation>().unwrap())
            .collect();
        Ok(LogicUnit {
            operations,
            input: vec![],
            x: 0,
            y: 0,
            w: 0,
            z: 0,
        })
    }
    pub fn reset(&mut self) {
        self.load_state(0, 0, 0, 0);
    }
    pub fn load_state(&mut self, x: i64, y: i64, w: i64, z: i64) {
        self.x = x;
        self.y = y;
        self.w = w;
        self.z = z;
    }

    pub fn get_state(&self) -> (i64, i64, i64, i64) {
        (self.x, self.y, self.w, self.z)
    }

    fn get_value(&self, var: VariablePlus) -> i64 {
        match var {
            VariablePlus::X => return self.x,
            VariablePlus::Y => return self.y,
            VariablePlus::W => return self.w,
            VariablePlus::Z => return self.z,
            VariablePlus::INT(s) => return s.clone(),
        }
    }

    fn input(&mut self, var: Variable) {
        match var {
            Variable::X => self.x = self.input.pop().unwrap(),
            Variable::Y => self.y = self.input.pop().unwrap(),
            Variable::W => self.w = self.input.pop().unwrap(),
            Variable::Z => self.z = self.input.pop().unwrap(),
        }
    }
    fn add(&mut self, a: Variable, b: VariablePlus) {
        match a {
            Variable::X => self.x += self.get_value(b),
            Variable::Y => self.y += self.get_value(b),
            Variable::W => self.w += self.get_value(b),
            Variable::Z => self.z += self.get_value(b),
        }
    }
    fn mul(&mut self, a: Variable, b: VariablePlus) {
        match a {
            Variable::X => self.x *= self.get_value(b),
            Variable::Y => self.y *= self.get_value(b),
            Variable::W => self.w *= self.get_value(b),
            Variable::Z => self.z *= self.get_value(b),
        }
    }
    fn div(&mut self, a: Variable, b: VariablePlus) {
        match a {
            Variable::X => self.x /= self.get_value(b),
            Variable::Y => self.y /= self.get_value(b),
            Variable::W => self.w /= self.get_value(b),
            Variable::Z => self.z /= self.get_value(b),
        }
    }
    fn rem(&mut self, a: Variable, b: VariablePlus) {
        match a {
            Variable::X => self.x %= self.get_value(b),
            Variable::Y => self.y %= self.get_value(b),
            Variable::W => self.w %= self.get_value(b),
            Variable::Z => self.z %= self.get_value(b),
        }
    }
    fn determine_equal(&self, a: i64, b: i64) -> i64 {
        if a == b {
            return 1;
        }
        0
    }
    fn eql(&mut self, a: Variable, b: VariablePlus) {
        match a {
            Variable::X => self.x = self.determine_equal(self.x, self.get_value(b)),
            Variable::Y => self.y = self.determine_equal(self.y, self.get_value(b)),
            Variable::W => self.w = self.determine_equal(self.w, self.get_value(b)),
            Variable::Z => self.z = self.determine_equal(self.z, self.get_value(b)),
        }
    }

    fn process_operations(&mut self, operation_index: usize) {
        match self.operations[operation_index] {
            Operation::INP(s) => self.input(s),
            Operation::ADD(a, b) => self.add(a, b),
            Operation::MUL(a, b) => self.mul(a, b),
            Operation::DIV(a, b) => self.div(a, b),
            Operation::MOD(a, b) => self.rem(a, b),
            Operation::EQL(a, b) => self.eql(a, b),
        }
    }
    pub fn run_input(&mut self, input: Vec<i64>) {
        self.input = input.into_iter().rev().collect();
        for x in 0..self.operations.len() {
            self.process_operations(x);
        }
    }
    pub fn validate(&self) -> bool {
        return self.z == 0;
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_variable() {
        assert_eq!("x".to_string().parse::<Variable>().unwrap(), Variable::X);
        assert_eq!("y".to_string().parse::<Variable>().unwrap(), Variable::Y);
        assert_eq!(
            "12".to_string().parse::<Variable>().unwrap_err(),
            LogicUnitError::InvalidVariableError
        );
        assert_eq!(
            "bla".to_string().parse::<Variable>().unwrap_err(),
            LogicUnitError::InvalidVariableError
        )
    }
    #[test]
    fn test_variable_plus() {
        assert_eq!(
            "x".to_string().parse::<VariablePlus>().unwrap(),
            VariablePlus::X
        );
        assert_eq!(
            "y".to_string().parse::<VariablePlus>().unwrap(),
            VariablePlus::Y
        );
        assert_eq!(
            "12".to_string().parse::<VariablePlus>().unwrap(),
            VariablePlus::INT(12)
        );
        assert_eq!(
            "-12".to_string().parse::<VariablePlus>().unwrap(),
            VariablePlus::INT(-12)
        );
        assert_eq!(
            "bla".to_string().parse::<VariablePlus>().unwrap_err(),
            LogicUnitError::InvalidVariableError
        )
    }
    #[test]
    fn test_operation() {
        assert_eq!(
            "inp x".to_string().parse::<Operation>().unwrap(),
            Operation::INP(Variable::X)
        );
        assert_eq!(
            "add x 3".to_string().parse::<Operation>().unwrap(),
            Operation::ADD(Variable::X, VariablePlus::INT(3))
        );
        assert_eq!(
            "xxx x 3".to_string().parse::<Operation>().unwrap_err(),
            LogicUnitError::InvalidOperationError
        );
        assert_eq!(
            "add b 3".to_string().parse::<Operation>().unwrap_err(),
            LogicUnitError::InvalidVariableError
        );
        assert_eq!(
            "add x".to_string().parse::<Operation>().unwrap_err(),
            LogicUnitError::InvalidOperationError
        );
    }
    #[test]
    fn test_logic_unit_1() {
        // check if input is one
        let mut lu = LogicUnit::new(&"inp z\nadd z -1".to_string()).unwrap();
        lu.run_input(vec![1]);
        assert_eq!(lu.validate(), true);
        lu.reset();
        lu.run_input(vec![2]);
        assert_eq!(lu.validate(), false);
    }
    #[test]
    fn test_logic_unit_2() {
        // check if second in put is 3 times larger
        let mut lu =
            LogicUnit::new(&"inp z\ninp x\nmul z 3\neql z x\nadd z -1".to_string()).unwrap();
        lu.run_input(vec![2, 6]);
        assert_eq!(lu.validate(), true);
        lu.reset();
        lu.run_input(vec![2, 3]);
        assert_eq!(lu.validate(), false);
        lu.reset();
        lu.run_input(vec![2, 10]);
        assert_eq!(lu.validate(), false);
    }
    #[test]
    fn test_logic_unit_3() {
        // check division and multiplication
        let mut lu =
            LogicUnit::new(&"inp x\ninp z\ndiv x 2\nmul x 4\n eql z x\nadd z -1".to_string())
                .unwrap();
        lu.run_input(vec![5, 8]);
        assert_eq!(lu.validate(), true);
        lu.reset();
        lu.run_input(vec![5, 2]);
        assert_eq!(lu.validate(), false);
    }
    #[test]
    fn test_logic_unit_4() {
        // Test remainder
        let mut lu =
            LogicUnit::new(&"inp x\ninp z\nmod x 7\neql z x\nadd z -1".to_string()).unwrap();
        lu.run_input(vec![21, 0]);
        assert_eq!(lu.validate(), true);
        lu.reset();
        lu.run_input(vec![23, 2]);
        assert_eq!(lu.validate(), true);
    }
}

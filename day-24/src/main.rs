use logic_unit::LogicUnit;
mod logic_unit;
use std::env;
use std::fs;
struct ModelNumberIterator {
    counter: u64,
}

impl ModelNumberIterator {
    pub fn new() -> Self {
        ModelNumberIterator {
            counter: 100000000000000,
        }
    }
}

impl Iterator for ModelNumberIterator {
    type Item = Vec<i64>;
    fn next(&mut self) -> Option<Vec<i64>> {
        let mut model_number: String;
        loop {
            self.counter -= 1;
            model_number = self.counter.to_string();
            if model_number.starts_with("0") {
                return None;
            }
            if !model_number.contains("0") {
                break;
            }
        }
        dbg!(&model_number);
        Some(
            model_number
                .chars()
                .map(|x| x.to_digit(10).unwrap() as i64)
                .collect(),
        )
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let mut lu = LogicUnit::new(&contents).unwrap();
    for input in ModelNumberIterator::new() {
        lu.run_input(input);
        if lu.validate() {
            break;
        }
    }
}

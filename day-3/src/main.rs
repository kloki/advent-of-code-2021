use std::convert::TryInto;
use std::env;
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Epsilon * Gamma {}", run_1(contents.clone()));
    println!("Life support {}", run_2(contents.clone()));
}
fn run_2(contents: String) -> isize {
    let lines: Vec<&str> = contents.trim().split("\n").collect();
    let size: usize = lines[0].chars().count().try_into().unwrap();
    let mut oxygen: String = "".to_string();
    let mut oxygen_vec = lines.to_vec();
    for pos in 0..size {
        let c = most_common(&oxygen_vec, pos);
        oxygen_vec.retain(|x| x.chars().nth(pos).unwrap() == c);
        if oxygen_vec.len() == 1 {
            oxygen = oxygen_vec[0].to_string();
            break;
        }
    }
    let mut scrubber: String = "".to_string();
    let mut scrubber_vec = lines.to_vec();
    for pos in 0..size {
        let c = least_common(&scrubber_vec, pos);
        scrubber_vec.retain(|x| x.chars().nth(pos).unwrap() == c);
        if scrubber_vec.len() == 1 {
            scrubber = scrubber_vec[0].to_string();
            break;
        }
    }
    to_integer(&oxygen) * to_integer(&scrubber)
}

fn run_1(contents: String) -> isize {
    let lines: Vec<&str> = contents.trim().split("\n").collect();
    let mut rate: Vec<i32> = vec![0; lines[0].chars().count().try_into().unwrap()];

    for line in &lines {
        for (i, c) in line.chars().enumerate() {
            if c == '1' {
                rate[i] += 1
            } else {
                rate[i] -= 1
            }
        }
    }
    let mut most: String = "".to_string();
    let mut least: String = "".to_string();
    for c in rate.iter() {
        if c < &0 {
            most += "0";
            least += "1";
        } else {
            most += "1";
            least += "0";
        }
    }
    to_integer(&most) * to_integer(&least)
}
fn to_integer(binary: &str) -> isize {
    return isize::from_str_radix(&binary, 2).unwrap();
}
fn count_bits(lines: &Vec<&str>, pos: usize) -> i32 {
    let mut counter: i32 = 0;
    for l in lines {
        if l.chars().nth(pos).unwrap() == '1' {
            counter += 1;
        } else {
            counter -= 1;
        }
    }
    return counter;
}

fn most_common(lines: &Vec<&str>, pos: usize) -> char {
    let count = count_bits(lines, pos);
    if count < 0 {
        return '0';
    }
    return '1';
}

fn least_common(lines: &Vec<&str>, pos: usize) -> char {
    let count = count_bits(lines, pos);
    if count >= 0 {
        return '0';
    }
    return '1';
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day3_1() {
        assert_eq!(
            run_1("00100\n11110\n10110\n10111\n10101\n01112\n00111\n11100\n10000\n11001\n00010\n01010".to_string()),
            198
        )
    }
    #[test]
    fn test_day3_2() {
        assert_eq!(
            run_2("00100\n11110\n10110\n10111\n10101\n01112\n00111\n11100\n10000\n11001\n00010\n01010".to_string()),
            230
        )
    }
}

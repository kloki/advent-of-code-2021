use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug)]
pub struct Board {
    items: [[Number; 5]; 5],
}
#[derive(Debug)]
pub struct Number {
    number: i32,
    checked: bool,
}
impl Copy for Number {}
impl Clone for Number {
    fn clone(&self) -> Number {
        Number {
            number: self.number,
            checked: self.checked,
        }
    }
}
impl Board {
    pub fn add_number(&mut self, number: &i32) {
        for y in 0..5 {
            for x in 0..5 {
                if self.items[x][y].number == *number {
                    self.items[x][y].checked = true;
                }
            }
        }
    }
    pub fn completed(&self) -> bool {
        for i in 0..5 {
            if (0..5).all(|x| self.items[i][x].checked) {
                return true;
            }
            if (0..5).all(|x| self.items[x][i].checked) {
                return true;
            }
        }
        false
    }
    pub fn result(&self) -> i32 {
        let mut amount = 0;
        for y in 0..5 {
            for x in 0..5 {
                if !self.items[x][y].checked {
                    amount += self.items[x][y].number;
                }
            }
        }
        amount
    }
}

impl FromStr for Board {
    type Err = BoardError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut items: [[Number; 5]; 5] = [[Number {
            checked: false,
            number: 0,
        }; 5]; 5];

        for (x, line) in s.split("\n").enumerate() {
            for (y, number) in line.split_whitespace().enumerate() {
                items[x][y].number = number.parse()?
            }
        }

        Ok(Self { items })
    }
}

#[derive(Debug)]
pub enum BoardError {
    InvalidNumber,
}
impl From<ParseIntError> for BoardError {
    fn from(_: ParseIntError) -> Self {
        Self::InvalidNumber
    }
}

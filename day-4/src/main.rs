use board::Board;
use std::env;
use std::fs;
mod board;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let (first, last) = run(contents);
    println!("First {}, Last {}", first, last)
}

fn run(contents: String) -> (i32, i32) {
    let lines: Vec<&str> = contents.trim().split("\n\n").collect();
    let numbers: Vec<i32> = lines[0]
        .split(",")
        .map(|s| s.parse::<i32>().unwrap())
        .collect();
    let mut boards: Vec<Board> = lines[1..]
        .iter()
        .map(|s| s.parse::<Board>().unwrap())
        .collect();
    let mut first: i32 = 0;
    let mut last: i32 = 0;
    for number in &numbers {
        for b in &mut boards {
            b.add_number(&number);
            if b.completed() {
                if first == 0 {
                    first = number * b.result();
                }
                last = number * b.result();
            }
        }
        boards.retain(|x| !x.completed());
    }
    (first, last)
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day4_1() {
        let (first, _) = run(input());
        assert_eq!(first, 4512)
    }
    #[test]
    fn test_day4_2() {
        let (_, last) = run(input());
        assert_eq!(last, 1924)
    }
    fn input() -> String {
        "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"
            .to_string()
    }
}

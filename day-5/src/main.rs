use std::collections::HashMap;
use std::env;
use std::fs;
use std::num::ParseIntError;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Simple: {}", run(contents.clone(), false));
    println!("Diagonal: {}", run(contents.clone(), true));
}

fn run(contents: String, include_diagonal: bool) -> i32 {
    let lines: Vec<&str> = contents.trim().split("\n").collect();
    let mut counts = HashMap::new();
    for x in &lines {
        let (a, b, c, d) = split_line(x).unwrap();
        if a == c {
            for i in line_range(b, d) {
                let key = format!("{}-{}", a, i);
                let entry = counts.entry(key).or_insert(0);
                *entry += 1;
            }
        } else if b == d {
            for i in line_range(a, c) {
                let key = format!("{}-{}", i, b);
                let entry = counts.entry(key).or_insert(0);
                *entry += 1;
            }
        } else if include_diagonal {
            for (i, j) in line_range(a, c).iter().zip(line_range(b, d)) {
                let key = format!("{}-{}", i, j);
                let entry = counts.entry(key).or_insert(0);
                *entry += 1;
            }
        }
    }
    let mut counter = 0;
    for (_, v) in &counts {
        if v > &1 {
            counter += 1;
        }
    }
    counter
}
fn split_line(line: &str) -> Result<(i32, i32, i32, i32), ParseIntError> {
    let line = line.replace(" -> ", ",");
    let words: Vec<&str> = line.split(",").collect();
    let a: i32 = words[0].parse::<i32>()?;
    let b: i32 = words[1].parse::<i32>()?;
    let c: i32 = words[2].parse::<i32>()?;
    let d: i32 = words[3].parse::<i32>()?;
    Ok((a, b, c, d))
}

fn line_range(a: i32, b: i32) -> Vec<i32> {
    if a < b {
        return (a..b + 1).collect();
    } else {
        return (b..a + 1).rev().collect();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day5_1() {
        assert_eq!(run(input(), false), 5)
    }
    #[test]
    fn test_day5_2() {
        assert_eq!(run(input(), true), 12)
    }

    fn input() -> String {
        "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"
            .to_string()
    }
}

#[derive(Debug)]
pub struct Fish {
    timer: u64,
    pub size: u64,
}
impl Fish {
    pub fn new(size: u64) -> Self {
        Fish { size, timer: 8 }
    }
    pub fn seed(timer: u64) -> Self {
        Fish { timer, size: 1 }
    }
    pub fn tick(&mut self) -> u64 {
        if self.timer == 0 {
            self.timer = 6;
            self.size
        } else {
            self.timer -= 1;
            0
        }
    }
}

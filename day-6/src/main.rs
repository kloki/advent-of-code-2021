use fish::Fish;
use std::env;
use std::fs;
mod fish;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let answer = run(contents.clone(), 50);
    println!("End population after 50 days {}", answer);
    let answer = run(contents, 256);
    println!("End population after 256 days {}", answer);
}

fn run(contents: String, iteration: u64) -> u64 {
    let mut fish: Vec<Fish> = contents
        .trim()
        .split(",")
        .map(|s| Fish::seed(s.parse::<u64>().unwrap()))
        .collect();
    let mut population: u64 = 0;
    for _ in 1..iteration + 1 {
        let new_fish: u64 = fish.iter_mut().map(|x| x.tick()).sum();
        fish.push(Fish::new(new_fish));
        let size: u64 = fish.iter().map(|x| x.size).sum();
        population = size;
    }
    population
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day6_1() {
        assert_eq!(run("3,4,3,1,2".to_string(), 80), 5934)
    }
    #[test]
    fn test_day6_2() {
        assert_eq!(run("3,4,3,1,2".to_string(), 256), 26984457539)
    }
}

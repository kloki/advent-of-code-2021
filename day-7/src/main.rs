use std::env;
use std::fs;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let answer = run(contents.clone(), fuel_simple);
    println!("Lowest fuel usage: {}", answer);
    let answer = run(contents, fuel_carl_guass);
    println!("Lowest fuel usage: {}", answer);
}

fn fuel_simple(distance: i64) -> i64 {
    distance
}

fn fuel_carl_guass(n: i64) -> i64 {
    return (n * (n + 1)) / 2;
}

fn run<F>(contents: String, fuel_calc: F) -> i64
where
    F: Fn(i64) -> i64,
{
    let positions: Vec<i64> = contents
        .trim()
        .split(",")
        .map(|s| s.parse::<i64>().unwrap())
        .collect();
    let min = positions.iter().min().unwrap();
    let max = positions.iter().max().unwrap();
    let mut results: Vec<i64> = Vec::new();
    for x in *min..*max + 1 {
        results.push(
            positions
                .iter()
                .map(|c| fuel_calc((c - x).abs()))
                .collect::<Vec<i64>>()
                .iter()
                .sum(),
        );
    }
    return *results.iter().min().unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day7_1() {
        assert_eq!(run("16,1,2,0,4,2,7,1,2,14".to_string(), fuel_simple), 37)
    }
    #[test]
    fn test_day7_2() {
        assert_eq!(
            run("16,1,2,0,4,2,7,1,2,14".to_string(), fuel_carl_guass),
            168
        )
    }
}

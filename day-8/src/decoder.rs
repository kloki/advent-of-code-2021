#[derive(Debug)]
pub struct Line<'contents> {
    pub pattern: Vec<&'contents str>,
    pub output: Vec<&'contents str>,
}
impl<'contents> Line<'contents> {
    pub fn new(s: &'contents str) -> Line<'contents> {
        let a: Vec<&str> = s.split(" | ").collect();
        let pattern: Vec<&str> = a[0].split(" ").collect();
        let output: Vec<&str> = a[1].split(" ").collect();
        Self { pattern, output }
    }
}

#[derive(Debug)]
pub struct Decoder {
    top: char,
    middle: char,
    down: char,
    upper_left: char,
    lower_left: char,
    upper_right: char,
    lower_right: char,
}

impl Decoder {
    pub fn new(s: Vec<&str>) -> Decoder {
        let mut one: Vec<char> = s
            .iter()
            .filter(|s| s.len() == 2)
            .next()
            .unwrap()
            .chars()
            .collect();
        let seven: Vec<char> = s
            .iter()
            .filter(|s| s.len() == 3)
            .next()
            .unwrap()
            .chars()
            .collect();
        let four: Vec<char> = s
            .iter()
            .filter(|s| s.len() == 4)
            .next()
            .unwrap()
            .chars()
            .collect();
        let eight: Vec<char> = s
            .iter()
            .filter(|s| s.len() == 7)
            .next()
            .unwrap()
            .chars()
            .collect();
        let top = disjoint(&one, &seven)[0];
        let three: Vec<char> = s
            .iter()
            .filter(|s| s.len() == 5)
            .filter(|s| s.contains(one[0]))
            .filter(|s| s.contains(one[1]))
            .next()
            .unwrap()
            .chars()
            .collect();
        let mut dis = disjoint(&four, &three);
        dis.retain(|&x| x != top);
        let down = dis[0];
        dis = disjoint(&four, &eight);
        dis.retain(|&x| x != top);
        dis.retain(|&x| x != down);
        let lower_left = dis[0];
        dis = disjoint(&three, &eight);
        dis.retain(|&x| x != lower_left);
        let upper_left = dis[0];

        dis = disjoint(&one, &three);
        dis.retain(|&x| x != upper_left);
        let middle = dis[0];

        let two: Vec<char> = s
            .iter()
            .filter(|s| s.len() == 5)
            .filter(|s| !s.contains(upper_left))
            .filter(|s| s.contains(lower_left))
            .next()
            .unwrap()
            .chars()
            .collect();

        let lower_right = disjoint(&two, &one)[0];

        one.retain(|&x| x != lower_right);
        let upper_right = one[0];
        Self {
            top,
            middle,
            down,
            upper_left,
            lower_left,
            upper_right,
            lower_right,
        }
    }

    pub fn decode(&self, output: Vec<&str>) -> i32 {
        (self.decode_output(output[0]) * 1000)
            + (self.decode_output(output[1]) * 100)
            + (self.decode_output(output[2]) * 10)
            + (self.decode_output(output[3]))
    }
    fn decode_output(&self, s: &str) -> i32 {
        match s.chars().count() {
            2 => 1,
            5 => {
                if !s.contains(self.lower_right) {
                    return 2;
                } else if !s.contains(self.upper_left) {
                    return 3;
                } else {
                    return 5;
                }
            }
            4 => 4,
            6 => {
                if !s.contains(self.upper_right) {
                    return 6;
                } else if !s.contains(self.lower_left) {
                    return 9;
                } else {
                    return 0;
                }
            }
            3 => 7,
            _ => 8,
        }
    }
}

fn disjoint<'a>(target: &Vec<char>, subject: &Vec<char>) -> Vec<char> {
    subject
        .iter()
        .filter(|l| !target.contains(l))
        .map(|l| *l)
        .collect()
}
#[test]
fn test_decoder() {
    let input = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab".to_string();

    let dec = Decoder::new(input.split(" ").collect());
    assert_eq!(dec.top, 'd');
    assert_eq!(dec.middle, 'f');
    assert_eq!(dec.down, 'c');
    assert_eq!(dec.upper_left, 'e');
    assert_eq!(dec.lower_left, 'g');
    assert_eq!(dec.upper_right, 'a');
    assert_eq!(dec.lower_right, 'b');
}

use decoder::{Decoder, Line};
use std::env;
use std::fs;
mod decoder;
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("Number of 1,4,7,8 are: {}", run_1(contents.clone()));
    println!("Sum of output: {}", run_2(contents.clone()));
}

fn run_1(contents: String) -> usize {
    let lines: Vec<Line> = contents.trim().split("\n").map(|s| Line::new(s)).collect();
    let mut counter = 0;
    for l in lines {
        for x in l.output {
            if let 2 | 4 | 3 | 7 = x.chars().count() {
                counter += 1
            }
        }
    }

    counter
}

fn run_2(contents: String) -> i32 {
    let lines: Vec<Line> = contents.trim().split("\n").map(|s| Line::new(s)).collect();
    let mut sum = 0;
    for l in lines {
        let dec = Decoder::new(l.pattern);
        sum += dec.decode(l.output);
    }

    sum
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day8_1() {
        assert_eq!(run_1(input()), 26)
    }
    #[test]
    fn test_day8_2() {
        assert_eq!(run_2(input()), 61229)
    }
    fn input() -> String {
        "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"
            .to_string()
    }
}

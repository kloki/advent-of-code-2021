def disjoint(x, y):
    dis = []
    for a in y:
        if a not in x:
            dis.append(a)
    return dis


def all_in(collection, target):
    for a in collection:
        if a not in target:
            return False
    return True


def remove(collection, remove):
    return [x for x in collection if x != remove]


patterns = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab".split()
for i in patterns:
    if len(i) == 2:
        print(f"{i} = 1")
        one = i
for i in patterns:
    if len(i) == 4:
        print(f"{i} = 4")
        four = i
for i in patterns:
    if len(i) == 3:
        print(f"{i} = 7")
        seven = i
for i in patterns:
    if len(i) == 7:
        print(f"{i} = 8")
        eight = i

upper_panel = disjoint(one, seven)[0]
print(f"upper_panel = {upper_panel}")

for i in patterns:
    if len(i) == 5 and all_in(one, i):
        print(f"{i} = 3")
        three = i

lower_panel = remove(disjoint(four, three), upper_panel)[0]
print(f"lower_panel = {lower_panel}")
lower_left = remove(remove(disjoint(four, eight), upper_panel), lower_panel)[0]
print(f"lower_left = {lower_left}")
upper_left = remove(disjoint(three, eight), lower_left)[0]
print(f"upper_left = {upper_left}")
middle = remove(disjoint(one, three), upper_left)[0]
print(f"middle = {middle}")
for i in patterns:
    if len(i) == 5 and upper_left not in i and i != three:
        print(f"{i} = 2")
        two = i
lower_right = disjoint(two, one)[0]
print(f"lower_right = {lower_right}")
upper_right = remove(one, lower_right)[0]
print(f"upper_right = {upper_right}")


# if len = 2 -> 1
# if len = 5 and not lower_right -> 2
# if len = 5 and not upper_left -> 3
# if len = 4 -> 4
# if len = 5 and not upper_right -> 5
# if len = 6 and not upper_right -> 6
# if len = 3 -> 7
# if len = 7 -> 8
# if len = 6 and not lower_left -> 9
# if len = 6 and not middle -> 0

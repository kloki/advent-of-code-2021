use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("low points {}", run_1(contents.clone()));
    println!("basins {}", run_2(contents));
}

fn low_points(floor: &Vec<Vec<usize>>) -> Vec<(usize, usize)> {
    let width = floor[0].len();
    let length = floor.len();
    let mut low_points: Vec<(usize, usize)> = Vec::new();
    for y in 0..width {
        for x in 0..length {
            if (y == 0 || floor[x][y] < floor[x][y - 1])
                && (y == width - 1 || floor[x][y] < floor[x][y + 1])
                && (x == 0 || floor[x][y] < floor[x - 1][y])
                && (x == length - 1 || floor[x][y] < floor[x + 1][y])
            {
                low_points.push((x, y));
            }
        }
    }
    low_points
}

fn grow_basin(floor: &mut Vec<Vec<usize>>, x: usize, y: usize) -> usize {
    let mut size: usize = 1;
    let width = floor[0].len();
    let length = floor.len();
    floor[x][y] = 9;
    if x != 0 && &floor[x - 1][y] < &9 {
        size += grow_basin(floor, x - 1, y)
    }
    if x != length - 1 && &floor[x + 1][y] < &9 {
        size += grow_basin(floor, x + 1, y)
    }
    if y != 0 && &floor[x][y - 1] < &9 {
        size += grow_basin(floor, x, y - 1)
    }
    if y != width - 1 && &floor[x][y + 1] < &9 {
        size += grow_basin(floor, x, y + 1)
    }
    size
}
fn run_1(contents: String) -> usize {
    let floor: Vec<Vec<usize>> = contents
        .trim()
        .split("\n")
        .map(|s| {
            s.split("")
                .filter(|x| x != &"")
                .map(|x| x.parse::<usize>().unwrap())
                .collect()
        })
        .collect();
    let lp = low_points(&floor);
    let height: usize = lp.iter().map(|(x, y)| floor[*x][*y]).sum();
    return lp.len() + height;
}

fn run_2(contents: String) -> usize {
    let mut floor: Vec<Vec<usize>> = contents
        .trim()
        .split("\n")
        .map(|s| {
            s.split("")
                .filter(|x| x != &"")
                .map(|x| x.parse::<usize>().unwrap())
                .collect()
        })
        .collect();
    let mut basins: Vec<usize> = low_points(&floor)
        .into_iter()
        .map(|(x, y)| grow_basin(&mut floor, x, y))
        .collect();
    basins.sort();
    basins[basins.len() - 3..]
        .iter()
        .fold(1usize, |tot, x| tot * x)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_day9_1() {
        assert_eq!(run_1(input()), 15)
    }
    #[test]
    fn test_day9_2() {
        assert_eq!(run_2(input()), 1134)
    }
    fn input() -> String {
        "2199943210
3987894921
9856789892
8767896789
9899965678
"
        .to_string()
    }
}

```
                _                         _
 _ __ _   _ ___| |_ _   _ ___  __ _ _ __ | |_ __ _
| '__| | | / __| __| | | / __|/ _` | '_ \| __/ _` |
| |  | |_| \__ \ |_| |_| \__ \ (_| | | | | || (_| |
|_|   \__,_|___/\__|\__, |___/\__,_|_| |_|\__\__,_|
                    |___/
```

Advent of code 2021 solutions written in rust.
https://adventofcode.com/2021

# Setup

Instal rust: https://rustup.rs/

# Test

Run test for all days. Input is based on the examples provided in the puzzles.

```
cargo test
```

# Run

x is the specific day to run. The input folder contains all input for my solutions.

```
cargo build --release
./target/release/day-<x> ./input/day<x>
```
